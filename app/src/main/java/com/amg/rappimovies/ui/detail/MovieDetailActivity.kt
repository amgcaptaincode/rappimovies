package com.amg.rappimovies.ui.detail

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.amg.rappimovies.R
import com.amg.rappimovies.constants.MovieConstants
import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.data.entities.Video
import com.amg.rappimovies.databinding.ActivityMovieDetailBinding
import com.amg.rappimovies.ui.MovieViewModel
import com.amg.rappimovies.ui.adapter.VideoAdapter
import com.amg.rappimovies.utils.Resource
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.appbar.CollapsingToolbarLayout
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailActivity : AppCompatActivity(), VideoAdapter.VideoListener {

    private val viewModel by viewModels<MovieViewModel>()
    private lateinit var binding: ActivityMovieDetailBinding

    private lateinit var adapter: VideoAdapter

    companion object {

        fun newIntent(activity: AppCompatActivity, transitionImage: View, movie: Movie) {
            val intent = Intent(activity, MovieDetailActivity::class.java)
            intent.putExtra(MovieConstants.EXTRA_MOVIE, movie)

            val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                activity,
                transitionImage,
                MovieConstants.EXTRA_MOVIE
            )
            ActivityCompat.startActivity(activity, intent, options.toBundle())

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        initActivityTransitions()
        setContentView(binding.root)

        initViews()

    }

    private fun fetchVideos(movie: Movie) {
        viewModel.fetchVideos(movie.id).observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    val videos = it.data
                    if (!videos.isNullOrEmpty()) adapter.setItems(ArrayList(videos))
                }
                Resource.Status.ERROR ->
                    Toast.makeText(baseContext, it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING ->
                    Toast.makeText(baseContext, getString(R.string.label_loading_trailers), Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun initViews() {

        ViewCompat.setTransitionName(
            binding.appBarLayoutDetail,
            MovieConstants.EXTRA_MOVIE
        )
        supportPostponeEnterTransition()

        binding.movieDetailToolbar.layoutParams =
            (binding.movieDetailToolbar.layoutParams as CollapsingToolbarLayout.LayoutParams).apply {
                topMargin = getStatusBarSize()
            }

        val movie = intent.getParcelableExtra<Movie>(MovieConstants.EXTRA_MOVIE) as Movie
        setSupportActionBar(binding.movieDetailToolbar)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back)
            title = movie.title
        }
        fetchVideos(movie)
        setupRecyclerView()
        showLabels(movie)
        showBackDrop(movie)

    }

    private fun setupRecyclerView() {
        binding.contentDetail.rvTrailers.layoutManager =
            LinearLayoutManager(baseContext, LinearLayoutManager.HORIZONTAL, false)
        adapter = VideoAdapter(this)
        binding.contentDetail.rvTrailers.adapter = adapter
    }

    private fun showLabels(movie: Movie) {

        binding.contentDetail.tvDetailTitle.text = movie.title
        binding.contentDetail.tvDetailDescription.text = movie.overview
        binding.contentDetail.tvDetailReleaseDate.text = movie.release_date
        binding.contentDetail.ratingBarDetailRating.rating = movie.vote_average / 2

    }

    fun showBackDrop(movie: Movie) {
        val path = if (movie.backdrop_path != null) {
            MovieConstants.getBackdropPath(movie.backdrop_path)
        } else if (movie.poster_path != null) {
            MovieConstants.getBackdropPath(movie.poster_path)
        } else ""

        Glide.with(baseContext)
            .load(path)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    supportStartPostponedEnterTransition()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    supportStartPostponedEnterTransition()
                    return false
                }

            })
            .transform(CenterCrop())
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(binding.ivMovieDetailPoster)
    }

    private fun getStatusBarSize(): Int {
        val idStatusBarHeight = resources.getIdentifier("status_bar_height", "dimen", "android")
        return if (idStatusBarHeight > 0) {
            resources.getDimensionPixelSize(idStatusBarHeight)
        } else 0
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) onBackPressed()
        return false
    }

    private fun initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val transition = Slide()
            transition.duration = 70L
            transition.excludeTarget(android.R.id.statusBarBackground, true)
            window.enterTransition = transition
            window.returnTransition = transition
        }
    }

    override fun onClickedItem(video: Video) {
        val intentYouTube =
            Intent(Intent.ACTION_VIEW, Uri.parse(MovieConstants.getYoutubeVideoPath(video.key)))
        startActivity(intentYouTube)
    }

}