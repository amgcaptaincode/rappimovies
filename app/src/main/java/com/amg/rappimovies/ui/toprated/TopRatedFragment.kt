package com.amg.rappimovies.ui.toprated

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.amg.rappimovies.R
import com.amg.rappimovies.constants.MovieConstants
import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.databinding.FragmentTopRatedBinding
import com.amg.rappimovies.ui.MovieViewModel
import com.amg.rappimovies.ui.adapter.MovieAdapter
import com.amg.rappimovies.ui.detail.MovieDetailActivity
import com.amg.rappimovies.utils.Resource
import com.amg.rappimovies.utils.autoCleared
import com.amg.rappimovies.utils.observeOnce
import com.softrunapps.paginatedrecyclerview.PaginatedAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TopRatedFragment : Fragment(), MovieAdapter.MovieListener {

    private var binding: FragmentTopRatedBinding by autoCleared()
    private val viewModel: MovieViewModel by viewModels()
    private lateinit var adapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTopRatedBinding.inflate(inflater, container, false)
        return  binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
        viewModel.postMovieTopRatePage(1)
    }

    private fun setupRecyclerView() {

        binding.rvTopRated.layoutManager = GridLayoutManager(requireContext(), 2)

        adapter = MovieAdapter(this)
        adapter.startPage = 1
        adapter.setPageSize(20)
        adapter.recyclerView = binding.rvTopRated
        //adapter.setDefaultRecyclerView(requireActivity(), binding.rvTopRated.id);

        //binding.rvTopRated.adapter = adapter
        adapter.setOnPaginationListener(object : PaginatedAdapter.OnPaginationListener {
            override fun onFinish() {
            }

            override fun onCurrentPage(page: Int) {
            }

            override fun onNextPage(page: Int) {
                viewModel.postMovieTopRatePage(page)
            }

        })

    }

    private fun setupObservers() {

        viewModel.movieTopRateListLiveData.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    val movies = it.data
                    if (!movies.isNullOrEmpty()) {
                        adapter.submitItems(ArrayList(movies))
                    }
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING ->
                    Toast.makeText(requireContext(), getString(R.string.label_loading_top_rated), Toast.LENGTH_SHORT).show()
            }
        })

    }

    override fun onClickedItem(view: View, movie: Movie) {
        MovieDetailActivity.newIntent(requireActivity() as AppCompatActivity, view, movie)
    }

}