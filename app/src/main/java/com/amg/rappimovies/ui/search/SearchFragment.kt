package com.amg.rappimovies.ui.search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.databinding.FragmentSearchBinding
import com.amg.rappimovies.ui.MovieViewModel
import com.amg.rappimovies.ui.adapter.MovieAdapter
import com.amg.rappimovies.ui.adapter.SearchMovieAdapter
import com.amg.rappimovies.ui.detail.MovieDetailActivity
import com.amg.rappimovies.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : Fragment(), SearchMovieAdapter.SearchMovieListener {

    private var binding: FragmentSearchBinding by autoCleared()
    private val viewModel: MovieViewModel by viewModels()
    private lateinit var adapter: SearchMovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()

        adapter = SearchMovieAdapter(this)

        binding.rvSearchMoview.layoutManager = GridLayoutManager(requireContext(), 3)
        binding.rvSearchMoview.adapter = adapter

    }

    private fun setupObservers() {

        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable?) {

            }

            override fun beforeTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (charSequence.isNullOrEmpty()) {
                    adapter.setItems(ArrayList<Movie>())
                } else {
                    viewModel.setSearchQuery(charSequence.toString())
                }
            }

        })

        viewModel.moviesLiveData.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Log.d("TAG", it.get(0).title)
                adapter.setItems(it as ArrayList<Movie>)
            }
        })

    }

    override fun onClickedItem(view: View, movie: Movie) {
        movie.category = ""
        MovieDetailActivity.newIntent(requireActivity() as AppCompatActivity, view, movie)
    }

}