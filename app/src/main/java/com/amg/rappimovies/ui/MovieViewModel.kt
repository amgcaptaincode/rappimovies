package com.amg.rappimovies.ui

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.amg.rappimovies.constants.MovieConstants
import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.data.repository.MovieRepository
import com.amg.rappimovies.utils.AbsentLiveData
import com.amg.rappimovies.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flatMapLatest

class MovieViewModel @ViewModelInject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    private var moviePopularLiveData: MutableLiveData<Int?> = MutableLiveData()
    val moviePopularListLiveData: LiveData<Resource<List<Movie>>>

    private var movieTopRateLiveData: MutableLiveData<Int?> = MutableLiveData()
    val movieTopRateListLiveData: LiveData<Resource<List<Movie>>>

    @ExperimentalCoroutinesApi
    private val searchChanel = ConflatedBroadcastChannel<String>()


    init {
        moviePopularListLiveData = moviePopularLiveData.switchMap {
            moviePopularLiveData.value?.let {
                repository.getMoviesByCategory(
                    MovieConstants.CATEGORY_POPULAR,
                    it
                )
            }
                ?: AbsentLiveData.create()
        }

        movieTopRateListLiveData = movieTopRateLiveData.switchMap {
            movieTopRateLiveData.value?.let {
                repository.getMoviesByCategory(
                    MovieConstants.CATEGORY_TOP_RATED,
                    it
                )
            }
                ?: AbsentLiveData.create()
        }
    }

    fun postMoviePopularPage(page: Int) = moviePopularLiveData.postValue(page)

    fun postMovieTopRatePage(page: Int) = movieTopRateLiveData.postValue(page)

    fun getMovies(category: String, page: Int): LiveData<Resource<List<Movie>>> =
        repository.getMoviesByCategory(category, page)

    fun fetchVideos(id: Int) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        val videos = repository.fetchVideos(id)
        if (videos != null) {
            emit(Resource.success(videos.data!!))
        } else {
            emit(Resource.error(message = "", data = null))
        }
    }

    @ExperimentalCoroutinesApi
    fun setSearchQuery(search: String) {
        searchChanel.offer(search)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    val moviesLiveData = searchChanel.asFlow()
        .flatMapLatest { query ->
            repository.getSearchMovies(query)
        }
        .catch { throwable ->
            Log.e("TAG", "throwable", throwable)
        }.asLiveData()

}