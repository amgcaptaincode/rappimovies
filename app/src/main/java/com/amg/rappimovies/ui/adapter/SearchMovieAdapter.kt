package com.amg.rappimovies.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amg.rappimovies.R
import com.amg.rappimovies.constants.MovieConstants
import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.databinding.ItemMovieBinding
import com.amg.rappimovies.databinding.ItemSearchMovieBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class SearchMovieAdapter(private val listener: SearchMovieListener) :
    RecyclerView.Adapter<SearchMovieViewHolder>() {

    interface SearchMovieListener {
        fun onClickedItem(view: View, movie: Movie)
    }

    private val items = ArrayList<Movie>()

    fun setItems(mItems: ArrayList<Movie>) {
        this.items.clear()
        this.items.addAll(mItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchMovieViewHolder {
        val binding: ItemSearchMovieBinding =
            ItemSearchMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchMovieViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: SearchMovieViewHolder, position: Int) =
        holder.bind(items[position])

    override fun getItemCount(): Int = items.size


}

class SearchMovieViewHolder(
    private val itemBinding: ItemSearchMovieBinding,
    private val listener: SearchMovieAdapter.SearchMovieListener
) : RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(item: Movie) {

        itemBinding.tvTitle.text = item.title
        itemBinding.tvDate.text = item.release_date

        item.backdrop_path?.let {
            Glide.with(itemBinding.root)
                .load(MovieConstants.getPosterPath(it))
                .transform(CenterCrop())
                .transition(DrawableTransitionOptions.withCrossFade())
                .error(R.drawable.shape_rectangle)
                .into(itemBinding.ivMovie)
        }

        itemBinding.root.setOnClickListener {
            listener.onClickedItem(itemBinding.ivMovie, item)
        }

    }

}