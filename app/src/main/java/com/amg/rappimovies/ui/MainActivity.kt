package com.amg.rappimovies.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.amg.rappimovies.ui.popular.PopularFragment
import com.amg.rappimovies.R
import com.amg.rappimovies.ui.search.SearchFragment
import com.amg.rappimovies.ui.toprated.TopRatedFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val searchFragment = SearchFragment()
        val topRatedFragment = TopRatedFragment()
        val popularFragment = PopularFragment()
        var activeFragment: Fragment = searchFragment

        supportFragmentManager.beginTransaction().apply {
            add(R.id.fl_layout, searchFragment, getString(R.string.label_search))
            add(R.id.fl_layout, topRatedFragment, getString(R.string.label_top_rated)).hide(topRatedFragment)
            add(R.id.fl_layout, popularFragment, getString(R.string.label_popular)).hide(popularFragment)
        }.commit()

        bottom_navigation_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_search -> {
                    setCurrentFragment(activeFragment, searchFragment)
                    activeFragment = searchFragment
                    true
                }
                R.id.menu_top_rated -> {
                    setCurrentFragment(activeFragment, topRatedFragment)
                    activeFragment = topRatedFragment
                    true
                }
                R.id.menu_popular -> {
                    setCurrentFragment(activeFragment, popularFragment)
                    activeFragment = popularFragment
                    true
                }
                else -> false
            }
        }

    }

    private fun setCurrentFragment(activateFragment: Fragment, fragmentToShow: Fragment) =
        supportFragmentManager.beginTransaction().hide(activateFragment).show(fragmentToShow)
            .commit()

}