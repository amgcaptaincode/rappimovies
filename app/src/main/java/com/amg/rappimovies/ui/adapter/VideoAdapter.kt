package com.amg.rappimovies.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amg.rappimovies.constants.MovieConstants
import com.amg.rappimovies.data.entities.Video
import com.amg.rappimovies.databinding.ItemVideoBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class VideoAdapter(private val listener: VideoListener) : RecyclerView.Adapter<VideoViewHolder>() {

    interface VideoListener {
        fun onClickedItem(video: Video)
    }

    private val items = ArrayList<Video>()

    fun setItems(mItems: ArrayList<Video>) {
        this.items.clear()
        this.items.addAll(mItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoViewHolder {
        val binding: ItemVideoBinding =
            ItemVideoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return VideoViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: VideoViewHolder, position: Int) {
        holder.bind(items[position])
    }

}

class VideoViewHolder(
    private val itemVideoBinding: ItemVideoBinding,
    private val listener: VideoAdapter.VideoListener
) : RecyclerView.ViewHolder(itemVideoBinding.root) {

    fun bind(item: Video) {

        Glide.with(itemVideoBinding.root)
            .load(MovieConstants.getYoutubeThumbnailPath(item.key))
            .transform(CenterCrop())
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(itemVideoBinding.ivVideo)

        itemVideoBinding.root.setOnClickListener {
            listener.onClickedItem(item)
        }

    }

}
