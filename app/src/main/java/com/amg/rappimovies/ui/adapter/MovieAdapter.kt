package com.amg.rappimovies.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amg.rappimovies.constants.MovieConstants
import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.databinding.ItemMovieBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.DrawableTransformation
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.softrunapps.paginatedrecyclerview.PaginatedAdapter

class MovieAdapter(private val listener: MovieListener) : PaginatedAdapter<Movie, MovieViewHolder>() {

    interface MovieListener {
        fun onClickedItem(view: View, movie: Movie)
    }

    private val items = ArrayList<Movie>()

    fun setItems(mItems: ArrayList<Movie>) {
        this.items.clear()
        this.items.addAll(mItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding: ItemMovieBinding =
            ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) =
        holder.bind(getItem(position))

}

class MovieViewHolder(
    private val itemBinding: ItemMovieBinding,
    private val listener: MovieAdapter.MovieListener
) : RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(item: Movie) {

        itemBinding.tvTitle.text = item.title
        itemBinding.tvDate.text = item.release_date

        item.backdrop_path?.let {
            Glide.with(itemBinding.root)
                .load(MovieConstants.getPosterPath(it))
                .transform(CenterCrop())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(itemBinding.ivMovie)
        }

        itemBinding.root.setOnClickListener {
            listener.onClickedItem(itemBinding.ivMovie, item)
        }

    }

}