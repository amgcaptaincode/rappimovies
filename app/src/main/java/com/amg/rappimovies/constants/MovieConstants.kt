package com.amg.rappimovies.constants

object MovieConstants {

    const val API_KEY = "ff1541ffb94b89e3dc599b860dec920d"
    const val API_LG = "en-US"
    const val API_URL = "https://api.themoviedb.org/3/"

    const val CATEGORY_POPULAR = "popular"
    const val CATEGORY_TOP_RATED = "top_rated"

    const val API_TIMEOUT = 45L

    private const val BASE_POSTER_PATH = "https://image.tmdb.org/t/p/w400"
    private const val BASE_BACKDROP_PATH = "https://image.tmdb.org/t/p/w780"
    private const val BASE_YOUTUBE_THUMBNAIL = "https://img.youtube.com/vi/"
    private const val BASE_YOUTUBE = "https://www.youtube.com/watch?v="

    const val MOVIE_DATABASE = "movie_db"
    const val EXTRA_MOVIE = "extra_movie"

    fun getPosterPath(posterPath: String): String {
        return BASE_POSTER_PATH + posterPath
    }

    fun getBackdropPath(backdropPath: String): String {
        return BASE_BACKDROP_PATH + backdropPath
    }

    fun getYoutubeThumbnailPath(thumbnailPath: String): String {
        return "$BASE_YOUTUBE_THUMBNAIL$thumbnailPath/default.jpg"
    }

    fun getYoutubeVideoPath(videoPath: String): String {
        return BASE_YOUTUBE.plus(videoPath)
    }

}