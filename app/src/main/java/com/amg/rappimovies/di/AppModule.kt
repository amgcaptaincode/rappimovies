package com.amg.rappimovies.di

import android.content.Context
import androidx.annotation.NonNull
import com.amg.rappimovies.constants.MovieConstants
import com.amg.rappimovies.data.local.MovieDao
import com.amg.rappimovies.data.local.MovieDatabase
import com.amg.rappimovies.data.remote.MovieRemoteDataSource
import com.amg.rappimovies.data.remote.MovieService
import com.amg.rappimovies.data.remote.RequestInterceptor
import com.amg.rappimovies.data.remote.ResponseInterceptor
import com.amg.rappimovies.data.repository.MovieRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, @NonNull okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(MovieConstants.API_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson() : Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun provideHttpClient() : OkHttpClient = OkHttpClient.Builder()
        .writeTimeout(MovieConstants.API_TIMEOUT, TimeUnit.SECONDS)
        .connectTimeout(MovieConstants.API_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(MovieConstants.API_TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(RequestInterceptor())
        .addInterceptor(ResponseInterceptor())
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()

    @Provides
    fun provideMovieService(retrofit: Retrofit) : MovieService = retrofit.create(MovieService::class.java)

    @Provides
    @Singleton
    fun provideMovieRemoteDataSource(movieService: MovieService) = MovieRemoteDataSource(movieService)

    @Provides
    @Singleton
    fun provideMovieDatabase(@ApplicationContext appContext: Context) = MovieDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideMovieDao(db: MovieDatabase) = db.movieDao()

    @Singleton
    @Provides
    fun provideRepository(
        remoteDataSource: MovieRemoteDataSource,
        localDataSource: MovieDao) = MovieRepository(remoteDataSource, localDataSource)

}