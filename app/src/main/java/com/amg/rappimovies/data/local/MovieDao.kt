package com.amg.rappimovies.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.amg.rappimovies.data.entities.Movie

@Dao
interface MovieDao {

    @Query("SELECT * FROM Movie WHERE page = :page AND category LIKE :category")
    fun getMoviesByCategory(category: String, page: Int): LiveData<List<Movie>>

    @Query("SELECT * FROM MOVIE WHERE id = :id")
    fun getMovieById(id: Int): Movie

    @Query("SELECT * FROM Movie WHERE page = :page")
    fun getMovieByPageList(page: Int): LiveData<List<Movie>>

    @Query("SELECT MAX(page) FROM Movie WHERE category = :category")
    fun getMaxPageByCategory(category: String) : LiveData<Int?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: List<Movie>)


}