package com.amg.rappimovies.data.remote.response

import com.amg.rappimovies.data.entities.Video

data class VideoListResponse(
    val id: Int,
    val results: List<Video>
)