package com.amg.rappimovies.data.remote.response

import com.amg.rappimovies.data.entities.Movie
import com.google.gson.annotations.SerializedName

data class MovieListResponse(
    val page: Int = 1,
    @SerializedName("total_results")
    val totalResults: Int = 0,
    @SerializedName("total_pages")
    val totalPages: Int = 0,
    @SerializedName("results")
    val results: List<Movie>
)