package com.amg.rappimovies.data.remote

import javax.inject.Inject

class MovieRemoteDataSource @Inject constructor(
    private val movieService: MovieService
) : BaseDataSource() {

    suspend fun getMoviesByCategory(category: String, page: Int) =
        getResponse { movieService.getMoviesByCategory(category, page) }

    suspend fun getMovieDetail(movieId: String) =
        getResponse { movieService.getMovieDetail(movieId) }

    suspend fun fetchVideos(movieId: Int) = getResponse { movieService.fetchVideos(movieId) }

    suspend fun getSearchMovies(query: String) = getResponse {
        movieService.getSearchMovies(query)
    }

    //suspend fun getSearchMovies(query: String) = movieService.getSearchMovies(query)


}