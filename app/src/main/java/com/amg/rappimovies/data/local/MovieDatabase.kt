package com.amg.rappimovies.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.amg.rappimovies.constants.MovieConstants
import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.utils.IntegerListConverter
import com.amg.rappimovies.utils.KeywordListConverter
import com.amg.rappimovies.utils.StringListConverter
import com.amg.rappimovies.utils.VideoListConverter

@Database(
    entities = [Movie::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(
    value = [
        (IntegerListConverter::class),
        (KeywordListConverter::class),
        (StringListConverter::class),
        (VideoListConverter::class)
    ]
)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun movieDao(): MovieDao

    companion object {

        @Volatile
        private var instance: MovieDatabase? = null

        fun getDatabase(context: Context): MovieDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context, MovieDatabase::class.java, MovieConstants.MOVIE_DATABASE)
                .fallbackToDestructiveMigration()
                .build()

    }

}