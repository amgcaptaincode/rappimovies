package com.amg.rappimovies.data.remote

import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.data.entities.Video
import com.amg.rappimovies.data.remote.response.VideoListResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    // Get Movies By Category (TopRated/Popular)
    @GET("movie/{category}")
    suspend fun getMoviesByCategory(
        @Path("category") category: String,
        @Query("page") page: Int
    ): Response<List<Movie>>


    // Get Movie detail by Id
    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(@Path("movie_id") movieId: String): Response<Movie>

    // Get Movie videos by Id
    @GET("movie/{movie_id}/videos")
    suspend fun fetchVideos(@Path("movie_id") id: Int): Response<List<Video>>

    @GET("search/movie")
    suspend fun getSearchMovies(@Query("query") query: String) : Response<List<Movie>>

}