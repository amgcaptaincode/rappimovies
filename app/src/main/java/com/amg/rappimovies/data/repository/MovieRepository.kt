package com.amg.rappimovies.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.amg.rappimovies.data.entities.Movie
import com.amg.rappimovies.data.local.MovieDao
import com.amg.rappimovies.data.remote.MovieRemoteDataSource
import com.amg.rappimovies.utils.Resource
import com.amg.rappimovies.utils.performGetOperation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val remoteDataSource: MovieRemoteDataSource,
    private val localDataSource: MovieDao
) {

    //suspend fun getMoviesByCategory(category: String, page: Int) = remoteDataSource.getMoviesByCategory(category, page)
    fun getMoviesByCategory(category: String, page: Int) = performGetOperation(
        databaseQuery = { localDataSource.getMoviesByCategory(category, page) },
        networkCall = { remoteDataSource.getMoviesByCategory(category, page) },
        saveCallResult = {
            for (item in it) {
                item.page = page
                item.category = category
            }
            localDataSource.insertAll(it)
        }
    )

    suspend fun fetchVideos(id: Int) = remoteDataSource.fetchVideos(id)

    // = remoteDataSource.getSearchMovies(query)
    suspend fun getSearchMovies(query: String) : Flow<List<Movie>?> {

        return flow {
            emit(remoteDataSource.getSearchMovies(query).data)
        }.flowOn(Dispatchers.IO)

    }

    fun getMaxPageByCategory(category: String) :LiveData<Int?> = localDataSource.getMaxPageByCategory(category)

}