# RappiMovies
This project is built with MVVM Architecture. Using the following dependencies:
- Architecture Components (Lifecycle, LiveData, ViewModel, Room Persistence, Coroutines)
- Material Components
- Dagger Hilt for dependency injection
- Retrofit2 for consumption of API REST The Movie DB
- Kotlin Kotlin Coroutines

# Data persistence
- Room Database was used
- MovieDao: It is the class in charge of retrieving data from the database.

# REST API consumption
- Retrofit 2 was used to make requests to the REST API.

#Repository
- MovieRepository is in charge of obtaining data from the RoomDatabase or from the REST API.

# ViewModel
- MovieViewModel is used to inform the UI of data changes through LiveData

# Dagger Hilt
- AppModule is in charge of injecting the Repository, RoomDatabase, Retrofit dependencies.

# UI
- In MainActivity BottomNavigationView was used to show fragments (Search / Top Rated / Popular
- In MovieDetailActivity a movie is shown in detail. In addition, trailers are shown that are viewed on YouTube.

# Unit Test
- To carry out unit tests I would have to study libraries like Mockito, JUnit, because I don't have enough knowledge to carry out this type of test.
